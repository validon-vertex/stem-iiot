# Validon Vertex #



### What is Validon Vertex? ###

**Validon Vertex** is an **IIoT** hardtech startup and **STEM** education & research software platform 
that provides data analytics; storage & backup; automatically curated AI/ML models; and AR/VR 3D model visualizations.